package com.lukeherron.test;

import com.lukeherron.ssio.facade.SpreadsheetFacade;
import com.lukeherron.ssio.Book;
import com.lukeherron.ssio.strategy.FormatStrategy;
import com.lukeherron.ssio.strategy.PhoneFormatStrategy;
import com.lukeherron.ssio.utility.WorkbookConverter;

import java.io.File;
import java.io.IOException;

public class Test {

    public static void main(String[] args) {

        File inputFile1 = new File("assets/realtest.xlsx");
        File inputFile2 = new File("assets/Book2003.xls");
        File inputFile3 = new File("assets/BookCSV.csv");

        File outputFile1 = new File("assets/OUTPUT1.xlsx");
        File outputFile2 = new File("assets/OUTPUT2.xls");
        File outputFile3 = new File("assets/OUTPUT3.csv");

        SpreadsheetFacade model = new SpreadsheetFacade();
        FormatStrategy strategy = new PhoneFormatStrategy();

        try {

            WorkbookConverter.Builder test = model.getSpreadsheet(inputFile1);

            Book book1 = model
                    .getSpreadsheet(inputFile1)
                    .formatColumn(0, 6, strategy)
                    .ignoreHeader()
                    .convert();

            Book book2 = model
                    .getSpreadsheet(inputFile2)
                    .formatColumn(0, 6, strategy)
                    .ignoreHeader()
                    .convert();

            Book book3 = model
                    .getSpreadsheet(inputFile3)
                    .formatColumn(0, 6, strategy)
                    .ignoreHeader()
                    .convert();

            book1.output(outputFile1);
            book2.output(outputFile2);
            book3.output(outputFile3);



        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
