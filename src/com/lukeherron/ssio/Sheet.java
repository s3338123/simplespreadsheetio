package com.lukeherron.ssio;

import java.util.List;
import java.util.ArrayList;

public class Sheet {

    private List<Row> rows = new ArrayList<>();

    public Row createRow() {

        Row row = new Row();
        this.rows.add(row);
        return row;
    }

    public Row createRow(String[] data) {

        Row row = new Row(data);
        this.rows.add(row);
        return row;
    }

    public List<Row> getRows() {

        return this.rows;
    }

    public List<String[]> toList() {

        List<String[]> data = new ArrayList<String[]>();

        for (Row row: rows) {
            data.add(row.getRow());
        }

        return data;
    }
}
