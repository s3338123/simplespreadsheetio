package com.lukeherron.ssio;

import com.lukeherron.ssio.spreadsheet.CSVSpreadsheet;
import com.lukeherron.ssio.spreadsheet.Excel2003Spreadsheet;
import com.lukeherron.ssio.spreadsheet.Excel2007Spreadsheet;
import com.lukeherron.ssio.spreadsheet.Spreadsheet;
import com.lukeherron.ssio.visitor.Visitor;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Book implements Visitor {

    private Spreadsheet spreadsheet;
    private Map<Integer, Sheet> sheets = new HashMap<>();
    private Integer activeSheet;

    public Book(Spreadsheet spreadsheet) {

        this.spreadsheet = spreadsheet;
    }

    public Sheet createSheet() {

        Sheet sheet = new Sheet();
        Integer currentSheet = sheets.size();
        this.sheets.put(currentSheet, sheet);
        this.activeSheet = currentSheet;

        return sheet;
    }

    public Sheet getSheet(int sheet) {

        return sheets.get(sheet);
    }

    public int getActiveSheet() {

        return this.activeSheet;
    }

    public void setActiveSheet(int activeSheet) {

        this.activeSheet = activeSheet;
    }

    public void output(File file) throws IOException {

        // Pass this class to the spreadsheets accept method. This will allow us to update this classes spreadsheet
        // variable with the correct type, without needing to know its type in advance. This ultimately replaces this
        // classes spreadsheet object with a new object containing all the current data from this Book instance (See
        // this classes visit methods to see how this is achieved)
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            spreadsheet.accept(this);
            spreadsheet.output(outputStream);
        }
    }

    @Override
    public void visit(Excel2003Spreadsheet spreadsheet) {

        // create HSSFWorkbook from this books data
        HSSFWorkbook workbook = new HSSFWorkbook();
        for (int i = 0; i < this.sheets.size(); i++) {
            String sheetName = spreadsheet.getWorkbook().getSheetName(i);
            HSSFSheet sheet = workbook.createSheet(sheetName);
            int j = 0;
            for (Row currentRow: this.sheets.get(i).getRows()) {
                HSSFRow row = sheet.createRow(j);
                int k = 0;
                for (String cellValue: currentRow.getRow()) {
                    HSSFCell cell = row.createCell(k);
                    cell.setCellValue(cellValue);
                    k++;
                }
                j++;
            }
        }

        this.spreadsheet = new Excel2003Spreadsheet(workbook);
    }

    @Override
    public void visit(Excel2007Spreadsheet spreadsheet) {

        // create XSSFWorkbook from this books data
        XSSFWorkbook workbook = new XSSFWorkbook();
        for (int i = 0; i < this.sheets.size(); i++) {
            String sheetName = spreadsheet.getWorkbook().getSheetName(i);
            XSSFSheet sheet = workbook.createSheet(sheetName);
            int j = 0;
            for (Row currentRow: this.sheets.get(i).getRows()) {
                XSSFRow row = sheet.createRow(j);
                int k = 0;
                for (String cellValue: currentRow.getRow()) {
                    XSSFCell cell = row.createCell(k);
                    cell.setCellValue(cellValue);
                    k++;
                }
                j++;
            }
        }

        this.spreadsheet = new Excel2007Spreadsheet(workbook);
    }

    @Override
    public void visit(CSVSpreadsheet spreadsheet) {

        // Create new CSV spreadsheet from this books data
        this.spreadsheet = new CSVSpreadsheet(this.sheets.get(0).toList());
    }
}
