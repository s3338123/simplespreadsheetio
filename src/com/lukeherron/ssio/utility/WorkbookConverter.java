package com.lukeherron.ssio.utility;

import com.lukeherron.ssio.*;
import com.lukeherron.ssio.spreadsheet.*;
import com.lukeherron.ssio.strategy.FormatStrategy;
import com.lukeherron.ssio.visitor.Visitor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

public class WorkbookConverter implements Visitor {

    private Spreadsheet spreadsheet;
    private Book book;
    private ArrayList<ColumnFormat> columnFormat = new ArrayList<>();
    private Integer dedupeColumn;
    private Boolean ignoreHeader = false;

    private WorkbookConverter(Builder builder) {

        this.spreadsheet = builder.spreadsheet;
        this.columnFormat = builder.columnFormat;
        this.dedupeColumn = builder.dedupeColumn;
        this.ignoreHeader = builder.ignoreHeader;
    }

    @Override
    public void visit(Excel2003Spreadsheet spreadsheet) {
        processPOIWorkbook(spreadsheet.getWorkbook());
    }

    @Override
    public void visit(Excel2007Spreadsheet spreadsheet) {
        processPOIWorkbook(spreadsheet.getWorkbook());
    }

    @Override
    public void visit(CSVSpreadsheet spreadsheet) {
        processList(spreadsheet.getWorkbook());
    }

    private void processPOIWorkbook(Workbook workbook) {

        this.book = new Book(this.spreadsheet);
        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            Sheet sheet = this.book.createSheet();
            Iterator rows = workbook.getSheetAt(i).rowIterator();

            while (rows.hasNext()) {
                Row row = (Row) rows.next();
                sheet.createRow(getPOICellData(row));
            }
        }
    }

    private String[] getPOICellData(Row row) {

        String[] cellData = new String[row.getLastCellNum()];

        for (int i = 0; i < cellData.length; i++) {
            Cell cell = row.getCell(i, Row.RETURN_NULL_AND_BLANK);
            if (cell == null) {
                cellData[i] = "";
            } else {
                switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_STRING:
                        cellData[i] = cell.getStringCellValue().trim();
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        //how do we deal with cells that contain scientific numbers?
                        cellData[i] = Long.toString((long)cell.getNumericCellValue()).trim();
                        break;
                    case Cell.CELL_TYPE_FORMULA:
                        cellData[i] = cell.toString().trim();
                        break;
                    case Cell.CELL_TYPE_BLANK:
                        cellData[i] = "";
                        break;
                    default:
                        cellData[i] = cell.toString().trim();
                }
            }
        }

        /*
         * Check to see if we have any format requests in the columnFormat array. If we do, check to see if that
         * formatting should be applied to the spreadsheet header. Format as appropriate if we don't have the header row
         */
        if (!columnFormat.isEmpty()) {
            if (row.getRowNum() != 0) {
                cellData = formatColumns(cellData);
            }
            else if (!ignoreHeader) {
                cellData = formatColumns(cellData);
            }
        }

        return cellData;
    }

    private void processList(List<String[]> workbook) {

        this.book = new Book(this.spreadsheet);
        Sheet sheet = this.book.createSheet();

        if (ignoreHeader) {
            sheet.createRow(workbook.get(0));
        }

        for (int i = ignoreHeader ? 1 : 0; i < workbook.size(); i++) {
            sheet.createRow(formatColumns(workbook.get(i)));
        }
    }

    /**
     * formatColumns method passes a string array 'cellData' to the format method of each ColumnFormat object
     * contained in the columnFormat arraylist. The output of each format method call is saved back to the
     * 'cellData' array
     *
     * @param cellData String array containing data to be formatted
     * @return formatted String array
     */
    private String[] formatColumns(String[] cellData) {

        for (ColumnFormat column: this.columnFormat) {
            if (column.getPage() == this.book.getActiveSheet() && column.getColumn() <= cellData.length) {
                cellData[column.getColumn()-1] = column.format(cellData[column.getColumn()-1]);
            }
        }

        return cellData;
    }

    public static class Builder {

        private Spreadsheet spreadsheet;
        private ArrayList<ColumnFormat> columnFormat = new ArrayList<>();
        private Integer dedupeColumn;
        private Boolean ignoreHeader = false;

        public Builder(Spreadsheet spreadsheet) {

            this.spreadsheet = spreadsheet;
        }

        /**
         * formatColumn stores formatStrategy objects and the relevant page and column to perform the formatting on
         *
         * @param page the page of the spreadsheet to perform formatting on
         * @param column the column of the page to perform formatting on
         * @param formatStrategy the formatStrategy to be called
         * @return SheetData.Builder object
         */
        public Builder formatColumn(int page, int column, FormatStrategy formatStrategy) {

            this.columnFormat.add(new ColumnFormat(page, column, formatStrategy));
            return this;
        }

        /**
         * Removes rows from the spreadsheet object based on duplicate matches found within the specified column
         *
         * @param column the column to be checked for duplicate data
         * @return SheetData.Builder object
         */
        public Builder dedupeColumn(int column) {

            this.dedupeColumn = column;
            return this;
        }

        /**
         * Specifies that the header is to be ignored when executing all other builder methods
         *
         * @return SheetData.Builder object
         */
        public Builder ignoreHeader() {

            this.ignoreHeader = true;
            return this;
        }

        /**
         * Creates and returns a SheetData instance
         *
         * @return SheetData instance, containing imported spreadsheet data
         */
        public Book convert() {

            WorkbookConverter visitor = new WorkbookConverter(this);
            spreadsheet.accept(visitor);
            return visitor.book;
        }
    }
}
