package com.lukeherron.ssio.factory;

import au.com.bytecode.opencsv.CSVReader;
import com.lukeherron.ssio.spreadsheet.CSVSpreadsheet;
import com.lukeherron.ssio.spreadsheet.Excel2003Spreadsheet;
import com.lukeherron.ssio.spreadsheet.Excel2007Spreadsheet;
import com.lukeherron.ssio.spreadsheet.Spreadsheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;

public class SpreadsheetReaderFactory {

    private static final String CSV_FILETYPE = "text/csv";
    private static final String EXCEL2003_FILETYPE = "application/vnd.ms-excel";
    private static final String EXCEL2007_FILETYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    public static Spreadsheet readSpreadsheet(File file) throws IOException {

        String fileType = Files.probeContentType(file.toPath());

        if (fileType.equals(EXCEL2003_FILETYPE) && file.getName().contains("csv")) {
            fileType = CSV_FILETYPE;
        }

        switch (fileType) {

            case CSV_FILETYPE:
                try (FileReader fileReader = new FileReader(file); CSVReader csvReader = new CSVReader(fileReader)) {
                    return new CSVSpreadsheet(csvReader.readAll());
                }

            case EXCEL2003_FILETYPE:
                try (FileInputStream fileStream = new FileInputStream(file)) {
                    return new Excel2003Spreadsheet(new HSSFWorkbook(fileStream));
                }

            case EXCEL2007_FILETYPE:
                try (FileInputStream fileStream = new FileInputStream(file)) {
                    return new Excel2007Spreadsheet(new XSSFWorkbook(fileStream));
                }

            default:
                throw new IllegalArgumentException("This file format can not be opened");
        }
    }
}
