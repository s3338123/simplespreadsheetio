package com.lukeherron.ssio.facade;

import com.lukeherron.ssio.spreadsheet.Spreadsheet;
import com.lukeherron.ssio.factory.SpreadsheetReaderFactory;
import com.lukeherron.ssio.utility.WorkbookConverter;

import java.io.File;
import java.io.IOException;

public class SpreadsheetFacade {

    public WorkbookConverter.Builder getSpreadsheet(File file) throws IOException {

        Spreadsheet spreadsheet = SpreadsheetReaderFactory.readSpreadsheet(file);
        return new WorkbookConverter.Builder(spreadsheet);
        //return SpreadsheetReaderFactory.readSpreadsheet(file);
    }
}
