package com.lukeherron.ssio.spreadsheet;

import com.lukeherron.ssio.visitor.Visitor;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Excel2007Spreadsheet extends AbstractSpreadsheet {

    private final XSSFWorkbook workbook;

    public Excel2007Spreadsheet(XSSFWorkbook workbook) {

        this(workbook, DEFAULT_SHEET);
    }

    public Excel2007Spreadsheet(XSSFWorkbook workbook, int activeSheet) {

        this.workbook = workbook;
        workbook.setActiveSheet(activeSheet);
    }

    @Override
    public XSSFWorkbook getWorkbook() {

        return this.workbook;
    }

    @Override
    public void setActiveSheet(int activeSheet) {

        this.workbook.setActiveSheet(activeSheet);
    }

    @Override
    public int getActiveSheet() {

        return this.workbook.getActiveSheetIndex();
    }

    @Override
    public void output(FileOutputStream fileStream) throws IOException {

        workbook.write(fileStream);
    }

    @Override
    public void accept(Visitor visitor) {

        visitor.visit(this);
    }
}
