package com.lukeherron.ssio.spreadsheet;

import au.com.bytecode.opencsv.CSVWriter;
import com.lukeherron.ssio.visitor.Visitor;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class CSVSpreadsheet extends AbstractSpreadsheet {

    private List<String[]> workbook;
    int activeSheetIndex = DEFAULT_SHEET;

    public CSVSpreadsheet(List<String[]> workbook) {

        this.workbook = workbook;
    }

    @Override
    public List<String[]> getWorkbook() {

        return this.workbook;
    }

    @Override
    public void setActiveSheet(int activeSheet) {

        // empty
    }

    @Override
    public int getActiveSheet() {

        return DEFAULT_SHEET;
    }

    @Override
    public void output(FileOutputStream fileStream) throws IOException {

        try (PrintWriter fileWriter = new PrintWriter(fileStream); CSVWriter writer = new CSVWriter(fileWriter)) {
            writer.writeAll(workbook);
        }
    }

    @Override
    public void accept(Visitor visitor) {

        visitor.visit(this);
    }
}
