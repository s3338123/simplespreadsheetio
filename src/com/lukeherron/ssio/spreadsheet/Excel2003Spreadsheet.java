package com.lukeherron.ssio.spreadsheet;

import com.lukeherron.ssio.visitor.Visitor;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Excel2003Spreadsheet extends AbstractSpreadsheet {

    private HSSFWorkbook workbook;

    public Excel2003Spreadsheet(HSSFWorkbook workbook) {

        this(workbook, DEFAULT_SHEET);
    }

    public Excel2003Spreadsheet(HSSFWorkbook workbook, int activeSheet) {

        this.workbook = workbook;
        workbook.setActiveSheet(activeSheet);
    }

    @Override
    public HSSFWorkbook getWorkbook() {

        return this.workbook;
    }

    @Override
    public void setActiveSheet(int activeSheet) {

        this.workbook.setActiveSheet(activeSheet);
    }

    @Override
    public int getActiveSheet() {

        return this.workbook.getActiveSheetIndex();
    }

    @Override
    public void output(FileOutputStream fileStream) throws IOException {

        workbook.write(fileStream);
    }

    @Override
    public void accept(Visitor visitor) {

        visitor.visit(this);
    }
}
