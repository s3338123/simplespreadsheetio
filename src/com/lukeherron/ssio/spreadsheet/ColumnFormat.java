package com.lukeherron.ssio.spreadsheet;

import com.lukeherron.ssio.strategy.FormatStrategy;

public class ColumnFormat {

    private int page;
    private int column;
    private FormatStrategy formatStrategy;

    public ColumnFormat(int page, int column, FormatStrategy formatStrategy) {

        this.page = page;
        this.column = column;
        this.formatStrategy = formatStrategy;
    }

    public int getPage() {

        return this.page;
    }

    public int getColumn() {

        return this.column;
    }

    public String format(String string) {

        return formatStrategy.format(string);
    }
}
