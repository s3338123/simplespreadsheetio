package com.lukeherron.ssio.spreadsheet;

import com.lukeherron.ssio.visitor.Visitable;

import java.io.FileOutputStream;
import java.io.IOException;

public interface Spreadsheet extends Visitable {

    Object getWorkbook();

    void setActiveSheet(int activeSheet);

    int getActiveSheet();

    void output(FileOutputStream fileStream) throws IOException;
}
