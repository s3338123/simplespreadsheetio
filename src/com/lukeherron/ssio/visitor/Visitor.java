package com.lukeherron.ssio.visitor;

import com.lukeherron.ssio.spreadsheet.CSVSpreadsheet;
import com.lukeherron.ssio.spreadsheet.Excel2003Spreadsheet;
import com.lukeherron.ssio.spreadsheet.Excel2007Spreadsheet;

public interface Visitor {

    public void visit(Excel2003Spreadsheet spreadsheet);

    public void visit(Excel2007Spreadsheet spreadsheet);

    public void visit(CSVSpreadsheet spreadsheet);
}
