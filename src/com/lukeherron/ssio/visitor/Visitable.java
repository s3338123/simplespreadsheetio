package com.lukeherron.ssio.visitor;

public interface Visitable {

    public void accept(Visitor visitor);
}
