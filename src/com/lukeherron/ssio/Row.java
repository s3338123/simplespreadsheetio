package com.lukeherron.ssio;

public class Row {

    private String[] row;

    public Row() {}

    public Row(String[] row) {

        this.row = row;
    }

    public String[] getRow() {

        return this.row;
    }
}
