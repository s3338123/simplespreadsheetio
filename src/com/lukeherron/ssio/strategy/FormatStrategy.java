package com.lukeherron.ssio.strategy;

public interface FormatStrategy {

    public String format(String string);
}
