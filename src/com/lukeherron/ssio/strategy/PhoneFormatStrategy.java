package com.lukeherron.ssio.strategy;

public class PhoneFormatStrategy implements FormatStrategy {

    private static final int CSV_NUMBER_LENGTH = 9;

    @Override
    public String format(String number) {

        // No point formatting an already valid number
        if (isValidNumber(number)) {
             return number;
        }

        // Remove anything that isn't a digit
        number = number.replaceAll("\\D", "");

        // If number starts with a 61, replace with 0
        if (number.startsWith("61")) {
            number = "0" + number.substring(2, number.length());
        }
        // If number is missing a 0 at the start, insert it
        else if (!number.startsWith("0") && number.length() == CSV_NUMBER_LENGTH) {
            number = "0" + number;
        }

        return number;
    }

    /**
     * A simple test that discerns if a provided number matches an Australian FNN
     *
     * @param number String value of the number to be checked
     * @return boolean value indicating validity of argument
     */
    public boolean isValidNumber(String number) {

        return number.matches("^([0])([23478])(\\d{8})$");
    }
}
